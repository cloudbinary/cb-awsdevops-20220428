
Dynamic Website : 2 Tier Architecture  [ Docker-Compose ]

Programming Language : Python

Client Side Programming Languages : HTML, CSS JS etc ( WebFramework : django )

Database : Postgresql

web : Container  : Python + Django 

DB : Container   : RDBMS (i.e. Postgresql )

File-1 : docker-compose.yml

File-2 : Dockerfile --> WebServer [ Python with Django ] & Download Postgresql from Hub.docker.com 

File-3 : requirements.txt

Create a Django project :

sudo docker-compose run web django-admin startproject eficensdevops . 


Connect the database


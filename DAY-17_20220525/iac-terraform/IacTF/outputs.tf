output "instance_id" {
  description = "Print Instance ID"
  value       = aws_instance.linux.id
}

output "instance_public_ip" {
  description = "Print Public IP"
  value       = aws_instance.linux.public_ip
}

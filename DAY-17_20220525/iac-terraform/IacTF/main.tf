# Provision EC2 Instance
resource "aws_instance" "linux" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = var.subnet_id
  vpc_security_group_ids = var.vpc_security_group_ids
  key_name               = var.key_name
  user_data              = file("web.sh")
  tags = {
    Name        = var.name
    Environment = "Development"
    ProjectName = "CloudBinary"
    ProjectID   = "20211204"
    CreatedBy   = "Iac-Terraform"
  }
}
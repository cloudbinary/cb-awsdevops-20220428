#!/bin/bash

# Launch AWS EC2 Instance Of Ubuntu 20.04
aws ec2 run-instances \
--image-id "ami-05ba3a39a75be1ec4" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-421fef29" \
--security-group-ids "sg-0e3746afaf53e4b3e" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=Linux-Ubuntu},{Key=Environment,Value=Dev}]' \
--key-name "aws-mumbai-keys" \
--user-data file://install-utilities.txt --profile devops

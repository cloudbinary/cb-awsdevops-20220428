Containers :

LXC :     https://linuxcontainers.org/
Linux Containers is an operating-system-level virtualization method for running multiple 
isolated Linux systems on a control host using a single Linux kernel

Containers

Elastic Container Registry --->  hub.docker.com 
 - Fully-managed Docker container registry : Share and deploy container software, publicly or privately


Elastic Container Service --> docker containers 
    - Highly secure, reliable, and scalable way to run containers


Elastic Kubernetes Service --> Kubernetes | DockerSwarm | Apache Mesos 
    - The most trusted way to start, run, and scale Kubernetes


Red Hat OpenShift Service on AWS
Fully managed Red Hat OpenShift service on AWS

Docker Commands :

# List images
docker images	

# List containers
docker ps	

# List Stopped or Running Containers 
docker ps -a 

# Display system-wide information
docker info

docker run -d --name CIServer -p 8080:8080 jenkins/jenkins:latest

Options :
--detach , -d		Run container in background and print container ID
--name		Assign a name to the container
--publish , -p		Publish a container's port(s) to the host
--publish-all , -P		Publish all exposed ports to random ports

Run a command in a running container :

Usage : 
 docker exec [OPTIONS] CONTAINER COMMAND [ARG...]

 docker exec -it dd9cc34e4609 cat /var/jenkins_home/secrets/initialAdminPassword


docker exec -it dd9cc34e4609 df -TH


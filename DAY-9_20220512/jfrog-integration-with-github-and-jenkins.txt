Continous Binary Code Repository :

    - JFrog / Nexus / AWS CodeArtifact / AZURE Artifact : 

Jfrog with Maven Integration 

https://jfrog.com/whitepaper/devops-8-reasons-for-devops-to-use-a-binary-repository-manager/
https://jfrog.com/
https://jfrog.com/artifactory/
https://www.sonatype.com/products/repository-pro
https://www.jfrog.com/confluence/display/JFROG/System+Requirements
https://aws.amazon.com/ec2/instance-types/

https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html
https://www.jfrog.com/confluence/display/JFROG/Maven+Artifactory+Plugin

https://maven.apache.org/settings.html
Q?

- Download, Install & Configure of Jenkins
- Updating Security Groups : 8080
- SSH To Jenkins Server 
- Accessing Jenkins UI using Browser

- Download, Install & Configure of Sonarqube 
- Updating Security Groups : 9000
- SSH To Sonarqube Server 
- Accessing Sonarqube UI using Browser

- Download, Install & Configure of Jfrog 
- Updating Security Groups : 8081 & 8082
- SSH To Jfrog Server 
- Accessing Jfrog UI using Browser

- Download, Install & Configure of Apache Tomcat 
- Updating Security Groups : 8080
- SSH To Tomcat Server 
- Accessing Tomcat UI using Browser



STEP-1 : Login to Jenkins Server 

Server :

- Java 
- Maven 

Env Variables :
- Java 
- Maven 

Jenkins Home Path  : /var/lib/jenkins/  

UI :
Env Variables :
- Java 
- Maven 

Jobs :
        - clean 
        - validate
        - compile
        - test
        - package
        - verify
        - install
        - deploy : deploy - done in the build environment, copies the final package to the remote repository for sharing with other developers and projects.
        - release

STEP-2 : Binary Code Repository - artifactory [Jfrog]

STEP-3 : Jfrog Integration with Jenkins :

- Do SSH To Jenkins Server 
- Go to Jenkins Home path : /var/lib/jenkins/.m2  
- Create or Find Maven home directory .m2 
- Download, Install & Configure Maven on Linux Server i.e. Jenkins 
- Default .m2 folder is available at root user home directory if not execute a maven goal part of Linux Server : mvn clean --> Inside the Code Repository
- Go to Maven Home path part of jenkins home : /var/lib/jenkins/.m2
    - Create 2 files :
        - settings.xml 
        - settings-security.xml 

    - Go to Jfrog UI & Generate Repository Path 
    - Copy the Repository Path & Credentials and update part of Jenkins Server part of Maven Home Path : /var/lib/jenkins/.m2/settings.xml 
    
    - Jenkins :
        - Encrypt Master Password --> settings-security.xml
        - Encrypt Password --> settings.xml
